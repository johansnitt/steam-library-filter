var gulp  = require('gulp'),
    minifycss = require('gulp-clean-css'),
    uglify = require('gulp-uglify'),
    plumber = require('gulp-plumber'),
    sass = require('gulp-sass'),
    rename = require('gulp-rename'),
    log = require('fancy-log'),
    newfile = require('gulp-file');
;

gulp.task('default', ['watch']);

gulp.task('watch', function() {
    gulp.watch('assets/sass/**/*.scss', ['build-css']);
    gulp.watch(['assets/js/*.js', '!assets/js/*.min.js'], ['build-js']);
});

const arg = (argList => {
    let arg = {}, a, opt, thisOpt, curOpt;
    for (a = 0; a < argList.length; a++) {
        thisOpt = argList[a].trim();
        opt = thisOpt.replace(/^\-+/, '');
        if (opt === thisOpt){
            if (curOpt) arg[curOpt] = opt;
            curOpt = null;
        }else {
            curOpt = opt;
            arg[curOpt] = true;
        }
    }
    return arg;
})(process.argv);


gulp.task('make-secret', function(){
            var fileName = "secret.php";
            var contents =  "<?php\n" +
                "defined('BASEPATH') OR exit('No direct script access allowed');\n" +
                "$secret = array();\n" +
                "\n" +
                "$secret['db_username'] = '" + arg.u + "';\n" +
                "$secret['db_password'] = '" + arg.p + "';\n" +
                "$secret['db_database'] = '" + arg.d + "';\n" +
                "\n" +
                "$secret['steam_api_key'] = '" + arg.s + "';";
            return newfile(fileName, contents)
                .pipe(gulp.dest('application/config'));
});

gulp.task('build-css', function() {
    return gulp.src('assets/sass/**/*.scss')
        .pipe(plumber())
        .pipe(sass())
        .on('error', function (err) {
            log(err);
            this.emit('end');
        })
        .pipe(minifycss({
            advanced: 'true',
            compatibility: 'ie8',
            keepSpecialComments: '0'
        }))
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest('assets/css/')).on('error', log);
});

gulp.task("build-js", function(){
    return gulp.src(['assets/js/*.js', '!assets/js/*.min.js'])
        .pipe(uglify())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest("assets/js/"));
});

gulp.task('build-all', ['build-css', 'build-js']);
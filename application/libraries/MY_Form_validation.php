<?php

class MY_Form_validation extends CI_Form_validation {

    public function __construct() {
        parent::__construct();
    }

    public function is_unique_not_deleted($str, $field){
        $pieces = explode('.', $field);
        return isset($this->CI->db)
            ? ($this->CI->db->limit(1)->get_where($pieces[0], array($pieces[1] => $str, $pieces[2].' !=' => 'deleted'))->num_rows() == 0)
        : FALSE;
    }

    public function is_unique_not_previous($str,$field){
        //is_unique_mk_3[users.".$user_id.".user_id.user_username.user_state]
        $pieces = explode(".",$field);
        if(isset($this->CI->db)){
            if($r = $this->CI->db->limit(1)->get_where($pieces[0], array($pieces[3] => $str, $pieces[4]." !=" => "deleted"))->result_array()){
                if(count($r) > 0){
                    if($r[0][$pieces[2]] == $pieces[1]){
                        return true;
                    }else{
                        return false;
                    }
                }
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    public function get_error_array(){
        return $this->_error_array;
    }

    public function get_error_array_remove_brackets(){
        $out = array();
        foreach($this->_error_array as $key => $value){
            $key = str_replace('[', '', $key);
            $key = str_replace(']', '', $key);
            $out[$key] = $value;
        }
        return $out;
    }
}
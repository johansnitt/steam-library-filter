<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Filter extends CI_Controller {

    protected $vars;

    public function __construct() {

        parent::__construct();
        $this->config->load('site',TRUE);
        $this->vars = $this->config->item('site');
        $this->vars['function'] = $this->router->fetch_method();
        $this->vars['class'] = $this->router->fetch_class();
        $this->vars['flashdata'] = $this->session->flashdata();
    }
    
    public function index(){
        $this->superparse();
    }

    public function search(){
        $query = $this->uri->segment(3, "");

        if($query != ""){
            if(preg_match("^[0-9]{17}$^", $query)){
                $id_user = $query;
            }else{
                $url = "http://api.steampowered.com/ISteamUser/ResolveVanityURL/v0001/?key=".$this->vars['steam_api_key']."&vanityurl=".$query;
                $result = $this->filtermodel->curl_return_json($url);

                if($result['response']['success'] == "1") {
                    $id_user = $result['response']['steamid'];
                }elseif($result['response']['success'] == "42"){
                    $this->session->set_flashdata('failed_post', $this->input->post());
                    $this->session->set_flashdata("fd_type", "warning");
                    $this->session->set_flashdata("fd_msg", "No match for this user.");
                    $this->hredir('/');
                }else{
                    $this->session->set_flashdata('failed_post', $this->input->post());
                    $this->session->set_flashdata("fd_type", "warning");
                    $this->session->set_flashdata("fd_msg", "Unknown error.");
                    $this->hredir('/');
                }
            }

            $url = "http://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/?key=".$this->vars['steam_api_key']."&steamid=".$id_user."&format=json&include_appinfo=1";
            $result = $this->filtermodel->curl_return_json($url);

            $this->vars['tags'] = array();
            $app_ids = array();
            $new_game_array = array();

            if(isset($result['response']['games']) AND !empty($result['response']['games'])){
                foreach ($result['response']['games'] as $game) {
                    $app_ids[] = $game['appid'];
                    $new_game_array[$game['appid']] = $game;
                }

                $in = '(' . implode(',', $app_ids) .')';
                $db_game_data = $this->db->query('SELECT * FROM games WHERE game_appid IN ' . $in)->result_array();

                foreach($db_game_data as $dgd){
                    if(empty($dgd['game_dev']) AND empty($dgd['game_pub']) AND empty($dgd['game_userscore'])){
                        unset($new_game_array[$dgd['game_appid']]);
                        continue;
                    }
                    $new_game_array[$dgd['game_appid']] = $this->filtermodel->format_and_populate_nga($dgd, $new_game_array[$dgd['game_appid']]);
                    foreach($new_game_array[$dgd['game_appid']]['raw_tags'] as $t){
                        $this->vars['tags'][] = $t;
                    }
                }
                $this->vars['games'] = $new_game_array;

            }else{
                $this->session->set_flashdata('failed_post', $this->input->post());
                $this->session->set_flashdata("fd_type", "warning");
                $this->session->set_flashdata("fd_msg", "No results found, check your privacy settings.");
                $this->hredir('/');
            }

            $this->vars['tags'] = array_unique($this->vars['tags']);
            asort($this->vars['tags']);
            uasort($this->vars['games'], function ($a, $b) {
                return strcmp($a['name'], $b['name']);
            });
        }
        $this->vars['page_title'] = $query.' - '.$this->vars['site_projectname'];
        $this->vars['query'] = $query;
        $this->superparse();
    }

    public function submit(){
        $this->hredir("/search/".trim($this->input->post('query')));
    }

    public function fetch_all_games(){
        $url = "http://steamspy.com/api.php?request=all";
        $result = $this->filtermodel->curl_return_json($url);
        $games = array();
        foreach($result as $r){
            $game_data['game_appid'] = $r['appid'];
            $game_data['game_name'] = $r['name'];
            $game_data['game_dev'] = $r['developer'];
            $game_data['game_pub'] = $r['publisher'];
            $game_data['game_userscore'] = $r['userscore'];
            $game_data['game_players_total'] = $r['players_forever'];
            $game_data['game_players_2weeks'] = $r['players_2weeks'];
            $games[] = $game_data;
        }

        if(!empty($games)){
            $this->db->truncate('games');
            $this->db->insert_batch('games', $games);
        }
    }

    public function refresh_game_tags(){
        set_time_limit(300);

        $games = $this->filtermodel->get_oldest_games(250);

        foreach($games->result() as $game){
            $this->filtermodel->get_and_update_tags($game);
        }
    }

    protected function header(){
        $this->parse('filter/common/header',$this->vars);
    }
    
    protected function footer() {
        $this->parse('filter/common/footer');
    }

    protected function superparse(){
        $this->header();
        $this->parse($this->vars['class']."/".$this->vars['function']);
        $this->footer();
    }

    protected function parse($tpl_path, $data_array = array()){
        return $this->parser->parse($tpl_path, $data_array);
    }

    protected function hredir($url) {
        header('Location: '.$url);
    }
}
?>

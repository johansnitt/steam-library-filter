<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require('secret.php');

$config['site_projectname'] = "Steam library filter";
$config['steam_api_key'] = $secret['steam_api_key'];
?>

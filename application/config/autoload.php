<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$autoload['packages'] = array();
$autoload['libraries'] = array('parser', 'database', 'session');
$autoload['drivers'] = array();
$autoload['helper'] = array('url');
$autoload['config'] = array('smarty');
$autoload['language'] = array();
$autoload['model'] = array('filtermodel');

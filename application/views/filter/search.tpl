
        {if $query}
            <div class="col-sm-8">
                <div class="form-group">
                    <label>Sort by:</label>
                    <select class="form-control" id="sort_select">
                        <option value="name">Name</option>
                        <option value="playtime">Playtime</option>
                        <option value="rating">User rating</option>
                        <option value="popularity">Most popular</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-16">
                <div class="form-group mt-2">
                    <label>Filter by tags:</label>
                    <ul class="list-unstyled tag-list fs-0" id="filter_list">
                        {foreach $tags as $tag}
                            <li class="active">{$tag}</li>
                        {/foreach}
                    </ul>
                </div>
                <div id="count" class="mb-1 text-right"></div>
            </div>
        {/if}
    </div>
        </div>
{if $query}
    <div id="content">
        <div id="results" class="row">
            {foreach $games as $game}
                {if isset($game.show) AND $game.show == 'true'}
                    <div class="col-sm-8 col-md-4 col-xl-2 pb-3 game_wrap" data-tags='{$game.game_tags_data_str}' data-name="{$game.name}" data-playtime="{$game.playtime_forever}" data-popularity="{$game.game_players_total}" data-rating="{$game.game_userscore}">
                        <img class="lazyload img-fluid" data-src="https://steamcdn-a.akamaihd.net/steam/apps/{$game.appid}/header.jpg" onerror="this.onerror=null;this.src='https://dummyimage.com/460x215/0000000/ffffff.jpg&text={urlencode($game.name)}';" title="{$game.name}" data-toggle="popover" data-trigger="hover" data-html="true" data-content="{$game.popover_content|escape}">
                    </div>
                {/if}
            {/foreach}
        </div>
    </div>
{/if}
<div id="loading_wrap">
    <i class="fas fa-spin fa-spinner-third fa-5x"></i>
</div>
<script src="/assets/js/search.min.js"></script>
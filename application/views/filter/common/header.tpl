<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="/assets/css/base.min.css" type="text/css" />
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-KwxQKNj2D0XKEW5O/Y6haRH39PE/xry8SAoLbpbCMraqlX7kUP6KHOnrlrtvuJLR" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="/assets/libraries/bootstrap/js/bootstrap.min.js"></script>
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <meta name="theme-color" content="#424242">
    <title>{$page_title|default:$site_projectname}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<div id="filter" class="container-fluid">
    <div class="filter-header row">
        <div class="col-sm-8">
            <i class="fab fa-fw fa-steam"></i>
            Steam library filter
        </div>
        <div class="col-sm-8 fs-0 text-right">
            <a href="https://github.com/johansnitt/steam-library-filter" target="_blank" class="btn btn-light" id="github-link"><i class="fab fa-github"></i> View source on Github</a>
        </div>
    </div>
    <div class="row">
        {if !empty($flashdata.fd_type)}
            <div class="alert alert-{$flashdata.fd_type}" role="alert">
                <i class="fa fa-fw fa-exclamation-triangle"></i> {$flashdata.fd_msg}
            </div>
        {/if}
        <div class="col-sm-8 {if !isset($query)}presearch{/if}">
            <form class="form-group" method="post" action="/index.php/filter/submit/">
                <label>Enter your SteamID or Custom URL (e.g. 76561198001776632 or johansnitt)</label>
                <div class="input-group">
                    <input type="text" class="form-control" name="query" placeholder="SteamID" value="{$flashdata.failed_post.query|default:$query|default}">
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="submit">Load library</button>
                    </div>
                </div>
            </form>
        </div>
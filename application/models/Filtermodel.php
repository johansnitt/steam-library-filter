<?php
class Filtermodel extends CI_Model {

    protected $vars;

    public function __construct() {
        parent::__construct();
    }

    public function curl_return_json($url){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        $result = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($result, TRUE);
        return $result;
    }

    public function format_and_populate_nga($dgd, $return){
        $return['show'] = 'true';
        $return['game_dev'] = $dgd['game_dev'] ?: '-';
        $return['game_pub'] = $dgd['game_pub'] ?: '-';
        $return['game_userscore'] = $dgd['game_userscore'] ?: 0;
        $return['game_players_total'] = $dgd['game_players_total'] ?: '-';
        $return['game_tags'] = json_decode($dgd['game_tags'], true) ?: array();
        $return['game_tags'] = array_slice($return['game_tags'], 0, 8);

        $return['game_tags_data_str'] = "";
        $return['raw_tags'] = array();
        $tag_str = "<ul class='list-unstyled tag-list fs-0 pt-1 mb-1'>";
        foreach ($return['game_tags'] as $tag_name => $tag_count) {
            $tag_name = str_replace('\'', '', $tag_name);
            $tag_name = str_replace('&', 'and', $tag_name);
            $return['raw_tags'][] = $tag_name;
            $tag_str .= "<li class='po'>" . $tag_name . "</li>";
            $return['game_tags_data_str'] .= $tag_name . ",";
        }
        $tag_str .= '</ul>';
        $new_game_array['game_tags_data_str'] = substr($return['game_tags_data_str'], 0, -1);

        $return['popover_content'] = "<ul class='list-unstyled'>
                                                    <li><strong>Developer:</strong> " . $return['game_dev'] . "</li>
                                                    <li><strong>Publisher:</strong> " . $return['game_pub'] . "</li>
                                                    <li><strong>User rating:</strong> " . $return['game_userscore'] . "/100</li>
                                                </ul>
                                                <strong>Tags:</strong> " . $tag_str;

        return $return;
    }

    public function get_oldest_games($limit){
        $this->db->select('game_id, game_appid');
        $this->db->order_by('game_tags_update_ts', 'ASC');
        $games = $this->db->get('games', $limit);
        return $games;
    }

    public function get_and_update_tags($game){
        $url = "http://steamspy.com/api.php?request=appdetails&appid=".$game->game_appid;
        $result = $this->curl_return_json($url);

        if(!empty($result['tags'])){
            $this->db->set('game_tags', json_encode($result['tags']));
            $this->db->set('game_tags_update_ts', time());
            $this->db->where('game_appid', $game->game_appid);
            $this->db->update('games');
        }else{
            $this->db->set('game_tags_update_ts', time());
            $this->db->where('game_appid', $game->game_appid);
            $this->db->update('games');
        }
    }

}
?>

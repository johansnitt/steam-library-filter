# Steam Library Filter

Tool for filtering a steam library using user generated tags. Code is provided "as is". Project made for fun in spare time. 

Live version can be found at https://steam.sni.tt/

## Running Locally

1. Create a new database
2. Run `npm install`
3. Get a steam api key https://steamcommunity.com/dev/apikey
4. Run `gulp make-secret -u (db username) -p (db password) -d (db name) -s (steam api key)`
5. Import data from steam.sql (optional I guess)

Some changes will need to be done and tasks need to be set up to keep game data up to date. Look at the functions and figure it out for yourself.

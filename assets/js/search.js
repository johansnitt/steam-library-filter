$(document).ready(function() {
    $('[data-toggle="popover"]').popover();
    search.init();
});

$('.lazyload').each( function(){
    $(this).attr( 'src', $(this).attr('data-src' ));
    $(this).removeAttr('data-src');
});

var search = {

    init: function (){
        this.cache_dom();
        this.bind_events();
        this.update_count();
        this.communicate_load_end();
    },

    cache_dom: function(){
        this.container = $('#results');
        this.games = $('#results').children('.game_wrap');
        this.filter_tags = $("#filter_list li");
        this.sort_select = $("#sort_select");
    },

    bind_events: function(){
        this.filter_tags.click(this.filter_by_tags.bind(this));
        this.sort_select.change(this.sort_games.bind(this));
    },

    update_count: function(){
        $('#count').html('Showing ' + search.games.not('.d-none').length + ' out of ' + search.games.length + ' games.');
    },

    filter_by_tags: function(e){
        this.communicate_load_start();
        this.filter_tags.removeClass('disabled');
        this.games.removeClass('d-none');
        $(e.target).toggleClass('selected_tag');
        var no_selected_tags = $('.selected_tag').length;
        if(no_selected_tags > 0){
            this.games.not('.d-none').each(function (game_k, game_v) {
                var applicable_tags = 0;
                var game_tags = $(game_v).data('tags').split(",");
                $(game_v).addClass('d-none');
                $('.selected_tag').each(function(tag_k, tag_v){
                    var filter_html = $(tag_v).html();
                    if(jQuery.inArray(filter_html, game_tags) !== -1){
                        applicable_tags = applicable_tags + 1;
                    }
                });
                if(no_selected_tags == applicable_tags){
                    $(game_v).removeClass('d-none');
                }
            });

            this.filter_tags.each(function(fl_k, fl_v){
                var filter_html = $(fl_v).html();
                $(fl_v).addClass('disabled');
                search.games.not('.d-none').each(function(){
                    var game_tags = $(this).data('tags').split(',');
                    if(jQuery.inArray(filter_html, game_tags) !== -1){
                        $(fl_v).removeClass('disabled');
                    }
                });
            });

        }else{
            this.games.removeClass('d-none');
        }
        this.update_count();
        this.communicate_load_end();
    },

    sort_games: function(){
        this.communicate_load_start();
        var type = this.sort_select.val();
        this.games.detach().sort(function(a, b) {
            var astts = $(a).data(type);
            var bstts = $(b).data(type)
            if(type == 'name'){
                return (astts > bstts) ? (astts > bstts) ? 1 : 0 : -1;
            }else{
                return (astts < bstts) ? (astts < bstts) ? 1 : 0 : -1;
            }
        });
        this.container.append(this.games);
        this.communicate_load_end();
    },

    communicate_load_start: function(delay) {
        var delay = delay || 0;
        $('#loading_wrap').removeClass('d-none');
        setTimeout(function() {
        }, delay);
    },

    communicate_load_end: function(delay) {
        var delay = delay || 250;
        setTimeout(function() {
            $('#loading_wrap').addClass('d-none');
        }, delay);
    }
};

